import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupColumnChartComponent } from './group-column-chart.component';

describe('GroupColumnChartComponent', () => {
  let component: GroupColumnChartComponent;
  let fixture: ComponentFixture<GroupColumnChartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GroupColumnChartComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupColumnChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
