import { Component, Input, ElementRef, ViewChild, OnInit } from "@angular/core";
declare var require: any;
import * as Highcharts from "highcharts";
import { Options } from 'highcharts';
const borderRadius = require("highcharts-border-radius");
borderRadius(Highcharts);
let Boost = require("highcharts/modules/boost");
let noData = require("highcharts/modules/no-data-to-display");
let More = require("highcharts/highcharts-more");

Boost(Highcharts);
noData(Highcharts);
More(Highcharts);
noData(Highcharts);
require("highcharts/modules/networkgraph")(Highcharts);

@Component({
  selector: 'app-group-column-chart',
  templateUrl: './group-column-chart.component.html',
  styleUrls: ['./group-column-chart.component.css']
})
export class GroupColumnChartComponent implements OnInit {

  @Input() groupData:any;
  @Input() categoryData:any;
  options:any;
  chart: any;

  constructor() {
    
  }

  ngOnInit() {
    this.init();
  }

  init(){
   
    this.options = {
      chart: {
        type: 'column'
      },
      title: {
        text: 'Group column chart'
      },
      xAxis: {
        categories: this.categoryData
      },
      yAxis: {
        min: 0,
      },
    credits: {
      enabled: false
    },
    legend: {
      align: 'left',
      verticalAlign: 'top',
      layout: 'horizontal'
  },
    plotOptions: {
      column: {
        borderRadiusTopLeft: 5,
        borderRadiusTopRight: 5
      },
      series:{
       groupPadding: 0.33,
       pointWidth: null,
       pointPadding:0.1

      }
    },
    
      series: this.groupData,
        
    };
    this.chart = Highcharts.chart("container", this.options);

  }
  
  
}
