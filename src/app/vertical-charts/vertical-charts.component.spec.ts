import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VerticalChartsComponent } from './vertical-charts.component';

describe('VerticalChartsComponent', () => {
  let component: VerticalChartsComponent;
  let fixture: ComponentFixture<VerticalChartsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VerticalChartsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VerticalChartsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
