
import { Component, Input, ElementRef, ViewChild, OnInit } from "@angular/core";
declare var require: any;
import * as Highcharts from "highcharts";

let Boost = require("highcharts/modules/boost");
let noData = require("highcharts/modules/no-data-to-display");
let More = require("highcharts/highcharts-more");

Boost(Highcharts);
noData(Highcharts);
More(Highcharts);
noData(Highcharts);
require("highcharts/modules/networkgraph")(Highcharts);

@Component({
  selector: 'app-vertical-charts',
  templateUrl: './vertical-charts.component.html',
  styleUrls: ['./vertical-charts.component.css']
})
export class VerticalChartsComponent implements OnInit {

  @Input() columnData:any;
  @Input() categoryData:any;
  categoryData1 =["value1","value1","value1","value1","value5"];
  columnData1 =[100,300,200,700,600];
  options:any;
  chart: any;
  colors = ['#6876C1'];

  constructor() {
    
  }

  ngOnInit() {
    this.init();
  }

  init(){
   

    this.options = {
      chart: {
        type: "column"
        
      },
      title: {
        text: 'Normal column chart'
      },
      colors:this.colors,
      xAxis: {
        categories: this.categoryData
      },
      yAxis: {
        min: 0
      },
      credits: {
        enabled: false
      },
      legend: {
        align: 'left',
        verticalAlign: 'top',
        layout: 'horizontal'
    },
      plotOptions: {
        column: {
          borderRadiusTopLeft: 5,
          borderRadiusTopRight: 5,
        },
        series:{
          groupPadding: 0.41,
          pointWidth: null,
        }
      },
      series: [
        {
          data: this.columnData
        }
      ]
    };
    this.chart = Highcharts.chart("container", this.options);

  }
  
  
}
