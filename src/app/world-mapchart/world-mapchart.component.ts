import { Component ,Input, OnInit} from "@angular/core";
import Highcharts from "highcharts/highmaps";
import worldMap from "@highcharts/map-collection/custom/world.geo.json";
import proj4 from "proj4";

@Component({
  selector: 'app-world-mapchart',
  templateUrl: './world-mapchart.component.html',
  styleUrls: ['./world-mapchart.component.css']
})
export class WorldMapchartComponent implements OnInit  {
  Highcharts: typeof Highcharts = Highcharts;
  chartConstructor = "mapChart";
  @Input() mapData:any;
  chartOptions:Highcharts.Options;

  ngOnInit() {
    this.init();
  }

  init(){
    this.chartOptions=  {
      chart: {
        map: worldMap,
        proj4: proj4
      },
      legend: {
        enabled: true
      },
      colorAxis: {
        visible: false
      },
      credits: {
        enabled: false
      },
      tooltip: {
        outside: true,
        useHTML: true,
        formatter: function () {
            return `<div class="tooltip">India,Maharashtra(30)</b></div>`;
        }
    },
      series: [
        {
          allAreas: true,
        
        } as Highcharts.SeriesMapOptions,
        {
          // Specify points using lat/lon
          type: "mappoint",
         
          marker: {
            radius: 3,
            fillColor: "red"
            
          },
          data: this.mapData
        }
      ]
    };
  }
}
