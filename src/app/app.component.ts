import { Component, Input, ElementRef, ViewChild, OnInit } from "@angular/core";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',

styleUrls: ["./app.component.css"]
})
export class AppComponent implements OnInit {
  title = "HighChartNetworkGraph";
  chart: any;
  isvisibleStackedChart:boolean=false;
  isvisibleGroupedChart:boolean=false;
  isvisible:boolean=false;
  isvisibleMap:boolean=false;
  isvisibleLineChart:boolean=false
  isvisibleSideNav:boolean= false;
  isvisibleRichEditor:boolean = false;
  constructor() {
    
  }
  categoryData :any[]=[];
  columnData:any[]=[];
  stackedData:any[] =[];
  groupData:any[] =[];
lineData:any[]=[];
mapData :any[]=[];


  ngOnInit() {
  
  }
  callStacked(){
    this.categoryData=["value1","value1","value1","value1","value5"];
    this.stackedData = [{
      name: 'valueBlueData',
      data: [400,900, 450, 100, 150],
      color:'#6876C1'
    }, {
      name: 'valuePurpleData',
      data: [100,300, 200, 150, 450],
      color:'#4C047C'

    }, {
      name: 'valueYellowData',
      data: [500,200, 250, 150, 200],
      color:'#FFD409'
    }, {
      name: 'valueRedData',
      data: [400,100, 200, 350, 300],
      color:'#C90E0B'
    }];

    this.isvisibleMap =false;
    this.isvisibleStackedChart=true;
    this.isvisible = false;
    this.isvisibleGroupedChart=false;
    this.isvisibleLineChart=false;
    this.isvisibleSideNav=false;
    this.isvisibleRichEditor=false;
  }
  callNormal(){
    this.categoryData=["value1","value1","value1","value1","value5"];
    this.columnData = [100,300,200,700,600];
    this.isvisible=true;
    this.isvisibleMap =false;
    this.isvisibleStackedChart=false;
    this.isvisibleGroupedChart=false;
    this.isvisibleLineChart=false;
    this.isvisibleSideNav=false;
    this.isvisibleRichEditor=false;

  }
  callGrouped(){
    this.categoryData=["value1","value1","value1","value1","value5"];
    this.groupData = [ {
      name: 'valueBlueData',
      data: [500,200, 250, 150, 200],
      color:'#314BB6'
      
    }, {
      name: 'valueLiteBlueData',
      data: [400,100, 200, 350, 300],
      color:'#6876C1'
    }];
    this.isvisibleMap =false;
    this.isvisible=false;
    this.isvisibleStackedChart=false;
    this.isvisibleGroupedChart=true;
    this.isvisibleLineChart=false;
    this.isvisibleSideNav=false;
    this.isvisibleRichEditor=false;

  }
  callLine(){
    this.categoryData=["value1","value1","value1","value1","value5"];
    this.lineData = [{
      name: 'valueBlueData',
      data: [400,900, 450, 100, 150],
      color:'#314BB6'
    },
    {
      name: 'valueYellowData',
      data: [400,900, 450, 100, 150],
      color:'#FFD409'
    }];
    this.isvisibleStackedChart=false;
    this.isvisible = false;
    this.isvisibleGroupedChart=false;
    this.isvisibleLineChart=true;
    this.isvisibleMap =false;
    this.isvisibleSideNav=false;
    this.isvisibleRichEditor=false;
  }
  callWorldMap(){
    this.isvisibleMap =true;
    this.isvisibleStackedChart=false;
    this.isvisible = false;
    this.isvisibleGroupedChart=false;
    this.isvisibleLineChart=false;
    this.isvisibleSideNav=false;
    this.isvisibleRichEditor=false;
    this.mapData=[
      {
        lat: 49.246292,
        lon: -123.116226,
      },
      {
        lat: 46.829853,
        lon: -71.254028,
      },
      {
        lat: 62.454,
        lon: -114.3718,
      }
    ]
  }

  callSideNav(){
    this.isvisibleSideNav=true;
    this.isvisibleMap =false;
    this.isvisibleStackedChart=false;
    this.isvisible = false;
    this.isvisibleGroupedChart=false;
    this.isvisibleLineChart=false;
    this.isvisibleRichEditor=false;
  }

  callRichEditor(){
    this.isvisibleSideNav=false;
    this.isvisibleMap =false;
    this.isvisibleStackedChart=false;
    this.isvisible = false;
    this.isvisibleGroupedChart=false;
    this.isvisibleLineChart=false
    this.isvisibleRichEditor=true;
  }
}