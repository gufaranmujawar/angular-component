import { Component, Input, ElementRef, ViewChild, OnInit } from "@angular/core";
declare var require: any;
import * as Highcharts from "highcharts";
import { Options } from 'highcharts';

let Boost = require("highcharts/modules/boost");
let noData = require("highcharts/modules/no-data-to-display");
let More = require("highcharts/highcharts-more");

Boost(Highcharts);
noData(Highcharts);
More(Highcharts);
noData(Highcharts);
require("highcharts/modules/networkgraph")(Highcharts);

@Component({
  selector: 'app-line-chart',
  templateUrl: './line-chart.component.html',
  styleUrls: ['./line-chart.component.css']
})
export class LineChartComponent implements OnInit {

  @Input() lineData:any;
  @Input() categoryData:any;
  options:any;
  chart: any;

  constructor() {
    
  }

  ngOnInit() {
    this.init();
  }

  init(){
   
    this.options = {
      chart: {
        type: 'line'
      },
      title: {
        text: 'Line chart'
      },
      xAxis: {
        categories: this.categoryData
      },
      yAxis: {
        min: 0,
      },
    credits: {
      enabled: false
    },
      plotOptions: {
        column: {
          borderRadiusTopLeft: 5,
          borderRadiusTopRight: 5,
        },
        series:{
          stacking: 'normal',
          groupPadding: 0.33,
          pointWidth: 35,
          marker: {
            symbol: 'circle'
          }
        }
      },
      legend: {
        align: 'left',
        verticalAlign: 'top',
        layout: 'horizontal'
    },
      
      series: this.lineData
    };
    this.chart = Highcharts.chart("container", this.options);

  }
  
  
}

