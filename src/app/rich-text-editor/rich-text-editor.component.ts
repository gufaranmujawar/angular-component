import { Component, OnInit } from '@angular/core';
import { AngularEditorConfig } from '@kolkov/angular-editor';

@Component({
  selector: 'app-rich-text-editor',
  templateUrl: './rich-text-editor.component.html',
  styleUrls: ['./rich-text-editor.component.css']
})
export class RichTextEditorComponent {

  htmlContent = '';
  drawer_head_title ='';
  selectedValue = null;
  inputValue: string = 'no data';
  category = ["Generic","Hacker's Mindset",'Incidence Response','Statistics','Attack'];
  note = { title: '', category: '', editorText: '' }
  config: AngularEditorConfig = {
                    editable: true,
                    spellcheck: true,
                    height: '12rem',
                    minHeight: '12rem',
                    width: '528px',
                    minWidth: '528px',
                    translate: "no",
                    defaultParagraphSeparator: 'p',
                    sanitize: false,
                    defaultFontName: 'Arial',
                    toolbarHiddenButtons: [
                                  [
                                    'strikeThrough',
                                    'subscript',
                                    'superscript'
                                  ]
                            ]
                   };

  visible = false;

  open(): void {
    this.visible = true;
    this.drawer_head_title ="Add New Note"
    this.note = {
      title: '',
      category: '',
      editorText: ''
    }
  }

  close(): void {
    this.visible = false;
  }

  save(): void {
    alert(this.note.title + this.note.category + this.note.editorText)
    this.visible = false;
  }
  submitted = false;

  onSubmit() { this.submitted = true; }

  edit(){
    this.visible = true;
    this.drawer_head_title ="Edit Note"
    this.note = {
      title: 'Title',
      category: 'Generic',
      editorText: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when unknown printer took a galley of type and scrambled it to make a type '
    }
  }
}
