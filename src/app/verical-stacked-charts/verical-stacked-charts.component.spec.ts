import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VericalStackedChartsComponent } from './verical-stacked-charts.component';

describe('VericalStackedChartsComponent', () => {
  let component: VericalStackedChartsComponent;
  let fixture: ComponentFixture<VericalStackedChartsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VericalStackedChartsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VericalStackedChartsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
