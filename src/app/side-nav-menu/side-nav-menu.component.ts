import { Component ,OnInit} from '@angular/core';

@Component({
  selector: 'app-side-nav-menu',
  templateUrl: './side-nav-menu.component.html',
  styleUrls: ['./side-nav-menu.component.css']
})
export class SideNavMenuComponent implements OnInit {
  isCollapsed:boolean = true;
  width:any =284;
  clickLink:boolean =false;
  clickDashboard:boolean =false;
  clickFinance:boolean =false;
  clickCyber:boolean =false;
  clickNews:boolean =false;
  clickPosture=false;
  clickSecurity:boolean =false;
  clickAttack:boolean =false;
  
  ngOnInit(){
  this.isCollapsed = true;
  this.width= 284;
  this.clickLink= false;
  this.clickDashboard=false;
  this.clickFinance= false;
  this.clickCyber=false;
  this.clickNews= false;
  this.clickPosture=false;
  this.clickSecurity= false;
  this.clickAttack= false;
}
  clickDashboardlink(){
    this.clickLink= false;
    this.clickDashboard=true;
    this.clickFinance= false;
    this.clickCyber=false;
    this.clickNews= false;
    this.clickPosture=false;
    this.clickSecurity= false;
    this.clickAttack = false;
  }
  clickfinancelink(){
    this.clickLink= false;
    this.clickDashboard=false;
    this.clickFinance= true;
    this.clickCyber=false;
    this.clickNews= false;
    this.clickPosture=false;
    this.clickSecurity= false; 
    this.clickAttack = false;
   }
  clickCyberlink(){
    this.clickLink= false;
    this.clickDashboard=false;
    this.clickFinance= false;
    this.clickCyber=true;
    this.clickNews= false;
    this.clickPosture=false;
    this.clickSecurity= false;
    this.clickAttack = false;
  }
  clickNewslink(){
    this.clickLink= false;
    this.clickDashboard=false;
    this.clickFinance= false;
    this.clickCyber=false;
    this.clickNews= true;
    this.clickPosture=false;
    this.clickSecurity= false
    this.clickAttack = false;
  }
  clickPosturelink(){
    this.clickLink= false;
    this.clickDashboard=false;
    this.clickFinance= false;
    this.clickCyber=false;
    this.clickNews= false;
    this.clickPosture=true;
    this.clickSecurity= false
    this.clickAttack = false;
  }
  clickSecuritylink(){
    this.clickLink= false;
    this.clickDashboard=false;
    this.clickFinance= false;
    this.clickCyber=false;
    this.clickNews= false;
    this.clickPosture=false;
    this.clickSecurity= true
    this.clickAttack = false;
  }
  clickAttacklink(){
    this.clickLink= false;
    this.clickDashboard=false;
    this.clickFinance= false;
    this.clickCyber=false;
    this.clickNews= false;
    this.clickPosture=false;
    this.clickSecurity= false;
    this.clickAttack = true;
  }
  
}
