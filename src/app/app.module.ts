
import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { FormsModule } from "@angular/forms";
import { AppComponent } from "./app.component";
import { VerticalChartsComponent } from './vertical-charts/vertical-charts.component';
import { ChartModule } from "angular-highcharts";
import { VericalStackedChartsComponent } from './verical-stacked-charts/verical-stacked-charts.component';
import { GroupColumnChartComponent } from './group-column-chart/group-column-chart.component';
import { LineChartComponent } from './line-chart/line-chart.component';
import { WorldMapchartComponent } from './world-mapchart/world-mapchart.component';
import { HighchartsChartModule } from 'highcharts-angular';
import { SideNavMenuComponent } from './side-nav-menu/side-nav-menu.component';
import { registerLocaleData } from '@angular/common';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { AngularEditorModule } from '@kolkov/angular-editor';

import { NZ_ICONS } from 'ng-zorro-antd/icon';
import { NZ_I18N, en_US } from 'ng-zorro-antd/i18n';
import { IconDefinition } from '@ant-design/icons-angular';
import * as AllIcons from '@ant-design/icons-angular/icons';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { DemoNgZorroAntdModule } from './side-nav-menu/ng-zorro-antd.module';
// registerLocaleData(en);




// import { registerLocaleData } from '@angular/common';
import {  ReactiveFormsModule } from '@angular/forms';
import { HttpClientJsonpModule, HttpClientModule } from '@angular/common/http';
import { RichTextEditorComponent } from './rich-text-editor/rich-text-editor.component';
// import en from '@angular/common/locales/en';

// registerLocaleData(en);


const antDesignIcons = AllIcons as {
  [key: string]: IconDefinition;
};
const icons: IconDefinition[] = Object.keys(antDesignIcons).map(key => antDesignIcons[key])
@NgModule({
  imports: [BrowserModule, FormsModule, ChartModule,HighchartsChartModule,
    DemoNgZorroAntdModule,
    BrowserAnimationsModule,
    ScrollingModule,
    DragDropModule,
    AngularEditorModule,
    HttpClientModule
  ],
  declarations: [AppComponent,SideNavMenuComponent,VerticalChartsComponent, VericalStackedChartsComponent, GroupColumnChartComponent, LineChartComponent, WorldMapchartComponent, SideNavMenuComponent, RichTextEditorComponent],
  bootstrap: [AppComponent,SideNavMenuComponent],
  providers: [ { provide: NZ_I18N, useValue: en_US }, { provide: NZ_ICONS, useValue: icons } ]
})
export class AppModule {}
